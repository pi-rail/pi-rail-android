# PI-Rail-Android

PI-Rail Android client based on PI-Mobile and PI-Rail-Client.

See [PI-Rail-FX](https://gitlab.com/pi-rail/pi-rail-fx) for documentation on desktop app and [PI-Rail-Arduino](https://gitlab.com/pi-rail/pi-rail-arduino) for documentation on firmware.

![Screenshot](doc/CTC-App-Android.png)

## Getting started

**If you just want to get the Client app for your CTC modules see [CTC download page](https://ctc-system.gitlab.io/ctc-doku/de/download.html).** 

If you want to start your own project or (better) contribute

1. Create a directory for your PI-Rail projects

2. Check out all PI-Mobile and PI-Rail projects to that directory.

   Check out by script:
   - Download the [Shell-Script](Checkout_PI-Rail-Android_Project.sh)
   - Create a directory e.g. PI-Rail
   - Copy the script inside the folder
   - If needed mark the script executable with chmod u-x (on Windows use the git-bash)
   - Run the script (on Windows use the git-bash)

   Check out by hand:
   Each project should be located in a directory having the name like the repository.
   You need the following directories (repositories):
   
   - [pi-rail-android](https://gitlab.com/pi-rail/pi-rail-android.git)
   - [pi-mobile-android](https://gitlab.com/pi-mobile/pi-mobile-android.git)
   - [pi-mobile-core](https://gitlab.com/pi-mobile/pi-mobile-core.git)
   - [pi-rail-base](https://gitlab.com/pi-rail/pi-rail-base.git)
   - [pi-rail-client](https://gitlab.com/pi-rail/pi-rail-client.git)
   - [Z21-Drive](https://github.com/PI-Data/z21-drive.git)


3. Open "pi-rail-androidApp" project with IntelliJ IDEA (do not open pi-rail-android itself).
    
    - Gradle is preconfigured to version 7.2.
    - Gradle is set to be used with JDK 11 in the Intellij settings.
    - Rename or copy the file [default_local.properties](pi-rail-androidApp/default_local.properties) to local.properties and add the path to your platform specific android sdk.
    - Set the project SDK in IntelliJ -> Project Structure -> Project -> SDK (we refer to use language level 8). 

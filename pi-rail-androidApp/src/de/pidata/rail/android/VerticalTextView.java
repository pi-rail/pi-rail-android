/*
 * This file is part of PI-Rail Android (https://gitlab.com/pi-rail/pi-rail-android).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatTextView;

public class VerticalTextView extends AppCompatTextView {

  private int width;
  private int height;
  private final Rect _bounds = new Rect();

  public VerticalTextView( Context context, AttributeSet attrs, int defStyle ) {
    super( context, attrs, defStyle );
  }

  public VerticalTextView( Context context, AttributeSet attrs ) {
    super( context, attrs );
  }

  public VerticalTextView( Context context ) {
    super( context );
  }

  @Override
  protected void onMeasure( int widthMeasureSpec, int heightMeasureSpec ) {
    super.onMeasure( widthMeasureSpec, heightMeasureSpec );
    // vise versa
    height = getMeasuredWidth();
    width = getMeasuredHeight();
    setMeasuredDimension( width, height );
  }

  @Override
  protected void onDraw( Canvas canvas ) {
    canvas.save();

    canvas.translate( width, height );
    canvas.rotate( -90 );

    TextPaint paint = getPaint();
    paint.setColor( getTextColors().getDefaultColor() );

    String text = text();

    paint.getTextBounds( text, 0, text.length(), _bounds );
    canvas.drawText( text, getCompoundPaddingLeft(), (_bounds.height() - width) / 2, paint );

    canvas.restore();
  }

  private String text() {
    return super.getText().toString();
  }
}
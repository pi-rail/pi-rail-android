/*
 * This file is part of PI-Rail Android (https://gitlab.com/pi-rail/pi-rail-android).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.android;

import android.os.Bundle;
import de.pidata.gui.android.activity.PiMobileActivity;

public class StatisticsActivity extends PiMobileActivity {

  public void onCreate( Bundle savedInstanceState ) {
    super.onCreate( savedInstanceState );
    createView( R.layout.statistics, NAMESPACE.getQName( "statistics" ) );
  }

  @Override
  public void requireRights() {
    // do nothing
  }

  /**
   * Called when pointer capture is enabled or disabled for the current window.
   *
   * @param hasCapture True if the window has pointer capture.
   */
  @Override
  public void onPointerCaptureChanged( boolean hasCapture ) {
    // do nothing
  }
}
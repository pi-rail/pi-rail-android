/*
 * This file is part of PI-Rail Android (https://gitlab.com/pi-rail/pi-rail-android).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.android;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.*;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.android.activity.PiMobileActivity;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;

public class PIRailActivity extends PiMobileActivity {

  // ListView OnItemClick //
  public ListView lv1;
  public ImageButton ib1;


  @Override
  protected void onCreate( Bundle savedInstanceState ) {
    super.onCreate( savedInstanceState );

    QName dialogName;
    String action = getIntent().getAction();
    if (action.equals( Intent.ACTION_MAIN )) {
      dialogName = NAMESPACE.getQName( "mobile_main" );
    }
    else {
      dialogName = NAMESPACE.getQName( getIntent().getAction() );
    }
    try {
      int layoutResID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_LAYOUT, dialogName );
      createView( layoutResID, dialogName );
    }
    catch (Exception ex) {
      String msg = "Error creating dialog name="+dialogName;
      Logger.error( msg, ex );
      throw new IllegalArgumentException( msg );
    }

    // Action Bar //
    ActionBar actionBar = getActionBar();
    actionBar.setLogo(R.drawable.pirail_storeicon_192x192);
    actionBar.setDisplayUseLogoEnabled( true ); //enable logo
    // actionBar.setDisplayHomeAsUpEnabled( true ); //enable back Button
    actionBar.setTitle( "CTC-App v"+BuildConfig.VERSION_NAME );
    actionBar.setBackgroundDrawable( new ColorDrawable( Color.parseColor("#090d13") ) );

    actionBar.show();
  }

  @Override
  public int getActionMenuLayoutID() {
    return R.menu.main_menu;

  }

  @Override
  public void requireRights() {
    // nothing to do
  }
}

/*
 * This file is part of PI-Rail Android (https://gitlab.com/pi-rail/pi-rail-android).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.android;

import android.util.ArrayMap;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.view.figure.*;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rect.*;

import java.util.*;

public class RailFigure extends AbstractFigure implements EventListener, PosSizeEventListener {

  private final ArrayList<ShapePI> shapes;

  protected RailFigure( Rect figureBounds, Model model ) {
    super( figureBounds );


    Map<Pos, List<Pos>> trackMap1 = new ArrayMap<>(  ); //<StartPos, EndPos>


    List<Pos> connList = new ArrayList<>(  );


    shapes = new ArrayList<>(  );

    //draw tracks
    for (Pos startPos : trackMap1.keySet()) {
      for (Pos endPos : trackMap1.get( startPos )) {
        LineShapePI lineShape = new LineShapePI( this, startPos, endPos, new ShapeStyle( ComponentColor.RED, 5 ) );
        shapes.add( lineShape );
      }
    }

    //draw connection as yellow squares
    for (Pos pos : connList) {
      RectangularShapePI rectShape = new RectanglePI( this, new RelativeRect( pos, -25, -25, 50, 50, 0 ), new ShapeStyle( ComponentColor.YELLOW, ComponentColor.TRANSPARENT, 5 ) );
      shapes.add( rectShape );
    }

  }

  @Override
  public int shapeCount() {
    return shapes.size();
  }

  @Override
  public ShapePI getShape( int index ) {
    if(shapes.size()>index) {
      return shapes.get( index );
    }
    else {
      return null;
    }
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {

  }

  @Override
  public void sizeChanged( Rect rect, double oldWidth, double oldHeight ) {

  }

  @Override
  public void posChanged( Pos pos, double oldX, double oldY ) {
    //do nothing
  }

  @Override
  public void rotationChanged( PosDir posDir, Rotation oldRotation ) {
    //do nothing
  }
}

/*
 * This file is part of PI-Rail Android (https://gitlab.com/pi-rail/pi-rail-android).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.android;

import android.app.Activity;
import android.content.Context;
import android.os.PowerManager;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.log.Logger;
import de.pidata.rail.client.swgrid.SwitchGridModule;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.*;

import java.util.Timer;
import java.util.TimerTask;

public class PiRailApplication extends AndroidApplication {

  private int i;

  protected PowerManager.WakeLock mWakeLock;

  /**
   * Called when the application is starting, before any other application
   * objects have been created.  Implementations should be as quick as
   * possible (for example using lazy initialization of state) since the time
   * spent in this function directly impacts the performance of starting the
   * first activity, service, or receiver in a process.
   * If you override this method, be sure to call super.onCreate().
   */
  @Override
  public void onCreate() {
    Logger.info( "PiRailApplication.onCreate()" );
    SwitchGridModule.cellSize = 80;
    super.onCreate();

    // make the screen be always on until this Activity gets destroyed.
    final PowerManager pm = (PowerManager) getSystemService( Context.POWER_SERVICE );
    this.mWakeLock = pm.newWakeLock( PowerManager.SCREEN_DIM_WAKE_LOCK, "CTC:App" );
    this.mWakeLock.acquire();

    // make sure PI-Rail is initialized before starting GUI
    PiRail.getInstance();

    //Checking connection status whether loco is connected or not.
    Timer myTimer = new Timer();
    myTimer.schedule( new TimerTask() {
      @Override
      public void run() {
        updateGUI();
      }
    }, 3000, 2000 );
    Logger.info( "finished PiRailApplication.onCreate()" );
  }

  @Override
  public void onActivityResumed( Activity activity ) {
    super.onActivityResumed( activity );
  }

  private void updateGUI() {
    i++;
    Platform.getInstance().runOnUiThread( myRunnable );
  }

  final Runnable myRunnable = new Runnable() {
    public void run() {
      ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
      for (Locomotive loco : modelRailway.locoIter()) {
        loco.refreshWlanIcon();
      }
    }
  };


  /**
   * This method is for use in emulated process environments.  It will
   * never be called on a production Android device, where processes are
   * removed by simply killing them; no user code (including this callback)
   * is executed when doing so.
   */
  @Override
  public void onTerminate() {
    this.mWakeLock.release();
    super.onTerminate();
  }
}


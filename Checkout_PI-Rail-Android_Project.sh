
#    HOWTO:
#    - Create a directory e.g. PI-Rail
#    - Copy the script inside the folder
#    - Run the script

pirailandroid="https://gitlab.com/pi-rail/pi-rail-android.git"
pimobileandroid="https://gitlab.com/pi-mobile/pi-mobile-android.git"
pimobilecore="https://gitlab.com/pi-mobile/pi-mobile-core.git"
pirailbase="https://gitlab.com/pi-rail/pi-rail-base"
pirailclient="https://gitlab.com/pi-rail/pi-rail-client"
Z21Drive="https://github.com/PI-Data/z21-drive.git"



declare -a repolist=("pi-rail-android::"$pirailandroid"@@master" "pi-mobile-android::"$pimobileandroid"@@master" "pi-mobile-core::"$pimobilecore"@@master" "pi-rail-base::"$pirailbase"@@master" "pi-rail-client::"$pirailclient"@@master" "z21-drive::"$Z21Drive"@@master" )

for dir in "${repolist[@]}";
do
    echo "=============================================================================="
    repoName="${dir%%::*}"
    _repoUrl="${dir%%@@*}"
    url="${_repoUrl##*::}"
    branchName="${dir##*@@}"
    targetdir="."
    
    echo "$repoName - $url - $branchName => $targetdir/$repoName"

    #mkdir -p "$targetdir"
    
    if [ -d "$repoName" ]
    then
        if [ "$(ls -A $repoName)" ]; then
            echo "Take action $repoName is not Empty"
            cd $(echo $repoName)
            git checkout $branchName
            git branch --list
            git pull
        else
            echo "$dir is Empty"
        fi
    else
       echo "######## STARTED CLONING $repoName ########"
       git clone -b $branchName $url $targetdir/$repoName
    fi

done
